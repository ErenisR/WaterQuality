package waterquality.erenis.com.waterquality;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

/**
 * Created by philipp on 02/06/16.
 */
public class DayAxisValueFormatter implements IAxisValueFormatter
{
    private List<String> mValues;

    private BarLineChartBase<?> chart;

    public DayAxisValueFormatter(BarLineChartBase<?> chart)
    {
        this.chart = chart;
    }

    public void setXAxisData(List<String> values)
    {
        mValues = values;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis)
    {
        return mValues.get((int) value);
    }
}