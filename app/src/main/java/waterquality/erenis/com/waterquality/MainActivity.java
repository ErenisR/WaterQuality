package waterquality.erenis.com.waterquality;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private static String TAG = MainActivity.class.getSimpleName();

    private TextView mResult;

    private BarChart mChart;

    private List<String> mLabels;

    private MainActivity getContext()
    {
        return MainActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLabels = new ArrayList<>();
        initViews();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 123:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    showFilePicker();
                }
                else
                {
                    ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
                }
                break;
        }
    }

    private int counter = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case 100:
                if (data != null)
                {
                    ClipData clipData = data.getClipData();
                    if (clipData != null)
                    {
                        for (int i = 0; i < clipData.getItemCount(); i++)
                        {
                            counter++;
                            ClipData.Item item = clipData.getItemAt(i);
                            try
                            {
                                File file = new File(item.getUri().getPath());
                                Log.d(TAG, "onActivityResult: " + file.exists());
                                if (file.exists())
                                {
                                    mLabels.add(file.getName());
                                }
                                else
                                {
                                    mLabels.add("File-" + counter);
                                }
                                InputStream is = getContentResolver().openInputStream(item.getUri());
                                if (is != null)
                                {
                                    List<String[]> list = read(is);
                                    validate(list, i);
                                }
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            counter++;
                            mLabels.add("File-" + counter);
                            InputStream is = getContentResolver().openInputStream(data.getData());
                            if (is != null)
                            {
                                List<String[]> list = read(is);
                                validate(list, mLabels.size() - 1);
                            }
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    setData(values.size());
                }
                break;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------

    private void initViews()
    {
        Button mButton = (Button) findViewById(R.id.widget_button_choose_file);
        mButton.setOnClickListener(mOnClickListener);
        mResult = (TextView) findViewById(R.id.widget_text_view_result);
        mResult.setMovementMethod(new ScrollingMovementMethod());
        mChart = (BarChart) findViewById(R.id.widget_chart);
        setupChart();
    }

    private void setupChart()
    {
        DayAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);
        xAxisFormatter.setXAxisData(mLabels);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(11);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(11, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(11, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
    }

    private void setData(int count)
    {
        float start = 1f;

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < count; i++)
        {
            float val = (float) values.get(i);

            yVals1.add(new BarEntry(i, val));

           /* if (Math.random() * 100 < 25)
            {
                yVals1.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.star)));
            }
            else
            {
                yVals1.add(new BarEntry(i, val));
            }*/
        }

        BarDataSet set1;

        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0)
        {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        }
        else
        {
            set1 = new BarDataSet(yVals1, "Files");

            set1.setDrawIcons(false);

            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);

            mChart.setData(data);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void showFilePicker()
    {
        /*Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select files"), 100);*/

        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 19)
        {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        }
        else
        {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] extraMimeTypes = {"text/*", "text/csv"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, 100);
    }

    public List<String[]> read(InputStream is) throws IOException
    {
        InputStreamReader inputStream = new InputStreamReader(is);
        List<String[]> resultList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(inputStream);
        try
        {
            String csvLine;
            while ((csvLine = reader.readLine()) != null)
            {
                Log.d(TAG, "Line: " + csvLine);
                String[] row = csvLine.split(",");
                resultList.add(row);
            }
        }
        catch (IOException ex)
        {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        finally
        {
            inputStream.close();
        }
        return resultList;
    }

    List<Integer> values = new ArrayList<>();

    private void validate(List<String[]> list, int index)
    {
        int wrongCounter = 0;
        int totalVariables = 0;
        List<Integer> variablesFailed = new ArrayList<>();
        List<Double> excursionValues = new ArrayList<>();
        String[] header = list.get(0);
        String[] objectives = list.get(list.size() - 1);
        if (header.length != objectives.length)
        {
            Toast.makeText(getContext(), "Uploaded document is not valid! The number of variables and objectives must be the same",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        for (int i = 1; i < list.size() - 1; i++)
        {
            String[] row = list.get(i);
            for (int j = 1; j < row.length; j++)
            {
                totalVariables++;
                String objectiveString = objectives[j];
                double lessThan = 0;
                double greatThan = 0;
                if (objectiveString.contains("<"))
                {
                    lessThan = Double.valueOf(objectiveString.substring(objectiveString.indexOf("<") + 1));
                    Log.d(TAG, "<: " + lessThan);
                }
                else if (objectiveString.contains(">"))
                {
                    greatThan = Double.valueOf(objectiveString.substring(objectiveString.indexOf(">") + 1));
                    Log.d(TAG, ">: " + greatThan);
                }
                else
                {
                    String[] values = objectiveString.split("-");
                    lessThan = Double.valueOf(values[1]);
                    greatThan = Double.valueOf(values[0]);
                    Log.d(TAG, lessThan + "-" + greatThan);
                }
                double var = Double.parseDouble(row[j]);
                if (lessThan == 0)
                {
                    if (var < greatThan)
                    {
                        wrongCounter++;
                        if (!variablesFailed.contains(j))
                        {
                            variablesFailed.add(j);
                        }
                        double excursion = calculateExcursion(var, greatThan);
                        excursionValues.add(excursion);
                    }
                }
                else if (greatThan == 0)
                {
                    if (var > lessThan)
                    {
                        wrongCounter++;
                        if (!variablesFailed.contains(j))
                        {
                            variablesFailed.add(j);
                        }
                        double excursion = calculateExcursion(var, lessThan);
                        excursionValues.add(excursion);
                    }
                }
                else
                {
                    if (var < greatThan || var > lessThan)
                    {
                        wrongCounter++;
                        if (!variablesFailed.contains(j))
                        {
                            variablesFailed.add(j);
                        }
                        if (var < greatThan)
                        {
                            double excursion = calculateExcursion(var, greatThan);
                            excursionValues.add(excursion);
                        }
                        if (var > lessThan)
                        {
                            double excursion = calculateExcursion(var, lessThan);
                            excursionValues.add(excursion);
                        }
                    }
                }
            }
        }
        float f1 = calculateF1orF2(variablesFailed.size(), objectives.length - 1);
        Log.d(TAG, "F1: " + f1);
        float f2 = calculateF1orF2(wrongCounter, totalVariables);
        Log.d(TAG, "F2: " + f2);
        float nse = calculateNSE(excursionValues, totalVariables);
        Log.d(TAG, "NSE: " + nse);
        float f3 = calculateF3(nse);
        Log.d(TAG, "F3: " + f3);

        int waterQuality = waterQuality(f1, f2, f3);
        Log.d(TAG, "Water quality: " + waterQuality);
        /*String res = mResult.getText().toString();
        mResult.setText(res.concat("\n ".concat(mLabels.get(index)).concat(": " + waterQuality)));*/
        values.add(waterQuality);
    }

    private float calculateF1orF2(int failedVariables, int totalVariables)
    {
        float res = (float) failedVariables / totalVariables;
        return res * 100;
    }

    private double calculateExcursion(double value, double objective)
    {
        if (value == 0 || objective == 0)
        {
            return -1;
        }
        if (value > objective)
        {
            return (value / objective) - 1;
        }
        else
        {
            return (objective / value) - 1;
        }
    }

    private float calculateNSE(List<Double> list, int totalValues)
    {
        float s = 0;
        for (double d : list)
        {
            s += d;
        }
        return s / totalValues;
    }

    private float calculateF3(float nse)
    {
        return (float) (nse / ((0.01 * nse) + 0.01));
    }

    private int waterQuality(float f1, float f2, float f3)
    {
        double d = Math.sqrt(f1 * f1 + f2 * f2 + f3 * f3);
        return (int) (100 - (d / 1.732));
    }

    //------------------------------------------------------------------------------------------------------------------------

    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int id = v.getId();

            switch (id)
            {
                case R.id.widget_button_choose_file:
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                            PackageManager.PERMISSION_GRANTED)
                    {
                        showFilePicker();
                    }
                    else
                    {
                        ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
                    }
                    break;
            }
        }
    };
}
