package waterquality.erenis.com.waterquality;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    @Override
    public String getFormattedValue(float value, AxisBase axis)
    {
        return mFormat.format(value);
    }

    public MyAxisValueFormatter()
    {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }
}
